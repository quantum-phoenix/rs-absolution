<footer class="footer-container">
    <div class="container">
        <div class="widget-area row">

            <div class="col span_3">
                <div class="widget text-widget">
                    <img class="margin-bottom" src="logo.png" alt="logo">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed suscipit metus. In sed risus leo, eu aliquet est.</p>
                    <div class="footer-social-icons small clr">
                        <a href="#">
                            <div class="social-icon-dribbble"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-facebook"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-skype"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-twitter"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-vimeo"></div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col span_3">
                <div class="widget widget_recent_entries">
                    <div class="title-small clr">
                        <h5>Recent <span class="text-bold">Posts</span></h5>
                    </div>
                    <ul>
                        <li><a href="#">Ut a diam id enim sodales</a></li>
                        <li><a href="#">Aliquam posuere, augue</a></li>
                        <li><a href="#">Vestibulum cursus elit placerat</a></li>
                        <li><a href="#">Pellentesque eu lobortis dolor</a></li>
                        <li><a href="#">Etiam gravida aliquet metus</a></li>
                    </ul>
                </div>
            </div>

            <div class="col span_3">
                <div class="title-small clr">
                    <h5>Latest <span class="text-bold">Tweets</span></h5>
                </div>
                <div class="tweet"></div>
            </div>

            <div class="col span_3">
                <div class="title-small clr">
                    <h5>Flickr <span class="text-bold">Feeds</span></h5>
                </div>
                <div class="flickr clr">
                    <div class="flickr-items">
                        <a href="http://www.flickr.com/photos/92831002@N03/8438240220/in/photostream/">
                            <img src="http://farm9.staticflickr.com/8073/8438240220_888436915b_s.jpg" alt="8438240220_888436915b_s">
                        </a>
                    </div>
                    <div class="flickr-items">
                        <a href="http://www.flickr.com/photos/92831002@N03/8438240216/in/photostream/">
                            <img src="http://farm9.staticflickr.com/8367/8438240216_b85e65240a_s.jpg" alt="8438240216_b85e65240a_s">
                        </a>
                    </div>
                    <div class="flickr-items">
                        <a href="http://www.flickr.com/photos/92831002@N03/8437157295/in/photostream/">
                            <img src="http://farm9.staticflickr.com/8330/8437157295_56b7e3a81c_s.jpg" alt="8437157295_56b7e3a81c_s">
                        </a>
                    </div>

                    <div class="flickr-items">
                        <a href="http://www.flickr.com/photos/92831002@N03/8437157347/in/photostream/">
                            <img src="http://farm9.staticflickr.com/8214/8437157347_88dc144594_s.jpg" alt="8437157347_88dc144594_s">
                        </a>
                    </div>
                    <div class="flickr-items">
                        <a href="http://www.flickr.com/photos/92831002@N03/8438240824/in/photostream/">
                            <img src="http://farm9.staticflickr.com/8506/8438240824_2471db03b2_s.jpg" alt="8438240824_2471db03b2_s">
                        </a>
                    </div>
                    <div class="flickr-items">
                        <a href="http://www.flickr.com/photos/92831002@N03/8438241190/in/photostream/">
                            <img src="http://farm9.staticflickr.com/8224/8438241190_b2660bc756_s.jpg" alt="8438241190_b2660bc756_s">
                        </a>
                    </div>

                    <div class="flickr-items">
                        <a href="http://www.flickr.com/photos/92831002@N03/8438241188/in/photostream/">
                            <img src="http://farm9.staticflickr.com/8085/8438241188_fda1ddf5cc_s.jpg" alt="8438241188_fda1ddf5cc_s">
                        </a>
                    </div>

                    <div class="flickr-items">
                        <a href="http://www.flickr.com/photos/92831002@N03/8438241292/in/photostream/">
                            <img src="http://farm9.staticflickr.com/8049/8438241292_f4c95dcd18_s.jpg" alt="8438241292_f4c95dcd18_s">
                        </a>
                    </div>

                    <div class="flickr-items">
                        <a href="http://www.flickr.com/photos/92831002@N03/8437158075/in/photostream/">
                            <img src="http://farm9.staticflickr.com/8218/8437158075_fb5c0fb8df_s.jpg" alt="8437158075_fb5c0fb8df_s">
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="copyright-container">
        <div class="container">
            <p class="white-text">© Copyright Absolution by<a target="blank" href="www.rubidium-style.com" class="green-hover"> Rubidium Style </a>- 2013</p>
        </div>
    </div>

</footer>
<div class="to-top" style="display: block;"></div>

</body>
</html>