<?php include 'header.php'; ?>

<section class="title-large">
    <div class="title-large-inner">
        <div class="container clr">
            <div class="row span_12 no-margin-col">

                <div class="col span_6">
                    <h2>Our <span class="text-bold"> Services</span></h2>                    
                </div>

                <div class="col span_6">
                    <ul class="inline-ul breadcrumb">
                        <li><a href="#">Pages</a></li>
                        <li><a href="our_services.php">Our services</a></li>                        
                    </ul>
                </div>

            </div>
        </div>      
    </div>            
</section>

<section class="container clr">

	<div class="row row-big-col">
	    <div class="col span_12">
	        <section class="slider">
	            <div class="flexslider about-us-slider">
	              <ul class="slides">
	                <li>
	                    <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/our-services/our_services_slider_1.jpg" alt="about_us_slider_1">
	                </li>
	                <li>
	                    <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/our-services/our_services_slider_2.jpg" alt="about_us_slider_2">
	                </li>
	              </ul>
	            </div>
	        </section>
	    </div>
	</div>

	<div class="row row-big-col">
	    <div class="col span_12">
			<div class="presentation-box">
                <img src="css/images/medium_content_box1.png" alt="medium_content_box1">
            	<h4 class="text-align-center text-bold">Responsive Layout</h4>
                <p class="text-align-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex esse!</p>
            </div>     
        
            <div class="presentation-box">
                <img src="css/images/medium_content_box2.png" alt="medium_content_box2">
                <h4 class="text-align-center text-bold">Clean Design</h4>
                <p class="text-align-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur placeat.</p>
            </div>       

            <div class="presentation-box">
                <img src="css/images/medium_content_box3.png" alt="medium_content_box3">
                <h4 class="text-align-center text-bold">Great Support</h4>
                <p class="text-align-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore laudantium.</p>
            </div>
        
            <div class="presentation-box">
                <img src="css/images/medium_content_box4.png" alt="medium_content_box4">
                <h4 class="text-align-center text-bold">Good Features</h4>
                <p class="text-align-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste nulla.</p>
            </div>
        </div>
	</div>

	<div class="row row-big-col">
	    <div class="col span_4">
	    	<div class="row row-big-col">
				<div class="col span_12">
			    	<div class="title-medium">
		                <h3>Why choose us?</h3>
		            </div>

					<div class="list circle">
			            <ul>
			                <li>Lorem ipsum dolor sit amet, consectetur.</li>
			                <li>Lorem ipsum dolor sit amet, consectetur.</li>
			                <li>Lorem ipsum dolor sit amet, consectetur.</li>
			                <li>Lorem ipsum dolor sit amet, consectetur.</li>
			                <li>Lorem ipsum dolor sit amet, consectetur.</li>
			                <li>Lorem ipsum dolor sit amet, consectetur.</li>
			                <li>Lorem ipsum dolor sit amet, consectetur.</li>
			            </ul>
			        </div>
			    </div>
			</div>
			<div class="row row-big-col">
			<div class="col span_12">
				<div class="title-medium">
	                <h3>What client's say?</h3>
	            </div>

				<section class="slider">
		            <div class="flexslider services-testimonials testimonials">
		              <ul class="slides">
		                <li>
		                  <div class="testimonials-author-div">
		                    <p class="testimonials-author-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt laborum quis ipsum ratione doloribus. Fugit nulla laborum dolorem minima soluta.</p>
		                  </div>
		                  <div class="testimonials-arrow"></div>
		            		  <div class="green text-align-center">
		                    <span class="testimonials-author">John Doe</span><span class="white-text"> / Web Design and Developer</span>
		                  </div>
		                </li>

		            		<li>
		            	    <div class="testimonials-author-div">
		                    <p class="testimonials-author-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt laborum quis ipsum ratione doloribus. Fugit nulla laborum dolorem minima soluta.</p>
		                  </div>
		                  <div class="testimonials-arrow"></div>
		                  <div class="green text-align-center">
		                    <span class="testimonials-author">John Doe</span><span class="white-text"> / Web Design and Developer</span>
		                  </div>
		            		</li>

		            		<li>
		            	    <div class="testimonials-author-div">
		                    <p class="testimonials-author-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt laborum quis ipsum ratione doloribus. Fugit nulla laborum dolorem minima soluta.</p>
		                  </div>
		                  <div class="testimonials-arrow"></div>
		                  <div class="green text-align-center">
		                    <span class="testimonials-author">John Doe</span><span class="white-text"> / Web Design and Developer</span>
		                  </div>
		            		</li>

		            		<li>
		            	    <div class="testimonials-author-div">
		                    <p class="testimonials-author-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt laborum quis ipsum ratione doloribus. Fugit nulla laborum dolorem minima soluta.</p>
		                  </div>
		                  <div class="testimonials-arrow"></div>
		                  <div class="green text-align-center">
		                    <span class="testimonials-author">John Doe</span><span class="white-text"> / Web Design and Developer</span>
		                  </div>
		            		</li>
		              </ul>
		            </div>
	          	</section>
		</div>			
	</div>

	    </div>
	    <div class="col span_8">
	    	<div class="tabs">
                <ul class="tab_navigation clr">
                    <li><a href="#tab-1">First Tab</a></li>
                    <li><a href="#tab-2">Second Tab</a></li>
                    <li><a href="#tab-3">Third tab</a></li>
                </ul>
                <div class="clr" id="tab-1">
                	<img class="floatleft img-margin" src="css/images/medium_content_tab_icon.png" alt="medium_content_tab_icon">
                	<span class="text-bold">Lorem ipsum dolor sit amet </span><span>Consectetur adipisicing elit. Nam possimus.</span>
                	<p>Mauris eu metus id lectus venenatis placerat. Nam mattis diam vitae lacus dictum quis dictum libero commodo. Etiam sagittis malesuada nisi eget vehicula. Nulla auctor mauris massa, et ornare odio. Mauris ultrices porta mauris ac suscipit. Cras congue elementum turpis, nec sodales turpis euismod sit amet. Phasellus vulputate semper nibh non hendrerit.</p>
                	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores expedita numquam. Possimus exercitationem quasi dolores iure quidem rerum tempore expedita ratione a ut! Quis alias sapiente expedita tenetur nam atque porro quas deleniti voluptates architecto minima nobis sunt temporibus consequuntur itaque sit totam voluptatibus animi laborum ratione perferendis perspiciatis vero!</p>

                </div>
                <div class="" id="tab-2">Cras congue elementum turpis, nec sodales turpis euismod sit amet. Phasellus vulputate semper nibh non hendrerit.
                </div>
                <div class="" id="tab-3">Mauris eu metus id lectus venenatis placerat. Cras congue elementum turpis, nec sodales turpis euismod sit amet. Phasellus vulputate semper nibh non hendrerit.
                </div>
            </div>
	    </div>
	</div>

</section>

<?php include 'footer.php'; ?>