<?php include 'header.php'; ?>

<section class="title-large">
    <div class="title-large-inner">
        <div class="container clr">
            <div class="row span_12 no-margin-col">

                <div class="col span_6">
                    <h2>Our <span class="text-bold"> Portfolio</span></h2>                    
                </div>

                <div class="col span_6">
                    <ul class="inline-ul breadcrumb">
                        <li><a href="portfolio.php">Portfolio</a></li>                        
                    </ul>
                </div>

            </div>
        </div>      
    </div>            
</section>

<section class="container clr">

    <div class="row">
        <div class="col span_12">
			<div class="options">
				<ul class="inline-ul option-set">
			        <li><a id="all" href="#">All</a></li>
			        <li><a id="illustration" href="#">Illustration</a></li>
			        <li><a id="art" href="#">Art</a></li>
			        <li><a id="abstract" href="#">Abstract</a></li>
			        <li><a id="photography" href="#">Photography</a></li>
			        <li><a id="video" href="#">Video</a></li>
			        <li><a id="print" href="#">Print</a></li>
	    		</ul>
			</div>
			<div id="portfolio">		    
			          
			    <div class="portfolio-element image-hover art">
			    	<img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/wonderland.jpg" alt="wonderland">
			    	
					<div class="portfolio-info">
			    		<p>Genesis</p>
			    		<span>art</span>,<span>illustration</span>
			    	</div>

			    	<div class="extra-info portfolio-image">
                        <a href="#" class="entry-img-hover-loupe"></a>
                        <a href="#" class="entry-img-hover-link"></a>
                    </div>
			    </div>

			    <div class="portfolio-element image-hover art">
			    	<img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/nanotech.jpg" alt="nanotech">
			    	
			    	<div class="portfolio-info">
			    		<p>Genesis</p>
			    		<span>art</span>,<span>illustration</span>
			    	</div>

			    	<div class="extra-info portfolio-image">
                        <a href="#" class="entry-img-hover-loupe"></a>
                        <a href="#" class="entry-img-hover-link"></a>
                    </div>
			    </div>

			    <div class="portfolio-element image-hover art abstract">
			    	<img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/intrinsi.jpg" alt="intrinsi">
			    	<div class="portfolio-info">
			    		<p>Genesis</p>
			    		<span>art</span>,<span>illustration</span>
			    	</div>

			    	<div class="extra-info portfolio-image">
                        <a href="#" class="entry-img-hover-loupe"></a>
                        <a href="#" class="entry-img-hover-link"></a>
                    </div>
			    </div>

			    <div class="portfolio-element image-hover illustration photography">
			    	<img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/genesis.jpg" alt="genesis">
			    	<div class="portfolio-info">
			    		<p>Genesis</p>
			    		<span>art</span>,<span>illustration</span>
			    	</div>

			    	<div class="extra-info portfolio-image">
                        <a href="#" class="entry-img-hover-loupe"></a>
                        <a href="#" class="entry-img-hover-link"></a>
                    </div>
			    </div>
			    
			    <div class="portfolio-element image-hover art">
			    	<img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/typowonder.jpg" alt="typowonder">
			    	
					<div class="portfolio-info">
			    		<p>Genesis</p>
			    		<span>art</span>,<span>illustration</span>
			    	</div>

			    	<div class="extra-info portfolio-image">
                        <a href="#" class="entry-img-hover-loupe"></a>
                        <a href="#" class="entry-img-hover-link"></a>
                    </div>
			    </div>

			    <div class="portfolio-element image-hover art">
			    	<img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/spaceseries.jpg" alt="spaceseries">
			    	
			    	<div class="portfolio-info">
			    		<p>Genesis</p>
			    		<span>art</span>,<span>illustration</span>
			    	</div>

			    	<div class="extra-info portfolio-image">
                        <a href="#" class="entry-img-hover-loupe"></a>
                        <a href="#" class="entry-img-hover-link"></a>
                    </div>
			    </div>

			    <div class="portfolio-element image-hover art video">
			    	<img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/fusion.jpg" alt="fusion">
			    	<div class="portfolio-info">
			    		<p>Genesis</p>
			    		<span>art</span>,<span>illustration</span>
			    	</div>

			    	<div class="extra-info portfolio-image">
                        <a href="#" class="entry-img-hover-loupe"></a>
                        <a href="#" class="entry-img-hover-link"></a>
                    </div>
			    </div>

			    <div class="portfolio-element image-hover print">
			    	<img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/fieldoflife.jpg" alt="fieldoflife">
			    	<div class="portfolio-info">
			    		<p>Genesis</p>
			    		<span>art</span>,<span>illustration</span>
			    	</div>

			    	<div class="extra-info portfolio-image">
                        <a href="#" class="entry-img-hover-loupe"></a>
                        <a href="#" class="entry-img-hover-link"></a>
                    </div>
			    </div>
			    
			</div>

			<div class="pager clr center">
				<a href="#" class="back-pager"></a>
				<ul>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">6</a></li>
				</ul>
				<a href="#" class="next-pager"></a>
			</div>


		</div>
    </div>

</section>

<?php include 'footer.php'; ?>