<div class="comments-div">

    <div class="comment clr comment-border">

        <div class="comment-avatar">
            <img src="css/images/medium_content_comments_avatar.png" alt="medium_content_comments_avatar">
        </div>
        <div class="comment-text">
            <div class="comment-box-inline clr">
                <h6 class="text-bold no-margin">Johny Joe</h6>
                <p>Feb 22, 2012 at 15:55</p>                
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec auctor, ligula at congue iaculis, magna quam hendrerit orci, eu placerat nisl sem id sem. Curabitur ut erat nisl. Mauris luctus, lacus non eleifend dignissim, quam lacus pellentesque massa, et vestibulum est quam id ipsum. Nam commodo luctus mauris.</p>
            <div class="reply-box">                    
                <a class="button-a" href="#">
                    <span class="comment-reply">Reply</span>
                </a>
            </div>
        </div>

    </div>

    <div class="comment-inner">
        <div class="comment clr">
            <div class="comment-avatar">
                <img src="css/images/medium_content_comments_avatar.png" alt="medium_content_comments_avatar">
            </div>
            <div class="comment-text">
                <div class="comment-box-inline clr">
                    <h6 class="text-bold no-margin">Johny Joe</h6>
                    <p>Feb 22, 2012 at 15:55</p>                    
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec auctor, ligula at congue iaculis, magna quam hendrerit orci, eu placerat nisl sem id sem. Curabitur ut erat nisl. Mauris luctus, lacus non eleifend dignissim, quam lacus pellentesque massa, et vestibulum est quam id ipsum. Nam commodo luctus mauris.</p>
                <div class="reply-box">                    
                    <a class="button-a" href="#">
                        <span class="comment-reply">Reply</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    
</div>