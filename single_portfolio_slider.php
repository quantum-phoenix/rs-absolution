<section class="slider jcarousel-single-portfolio-skin">                
      <ul class="jcarousel-skin-absolution jcarousel-single-portfolio">
        <li>
            <div class="portfolio-element image-hover art">
                <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/genesis.jpg" alt="genesis">
                
                <div class="portfolio-info">
                    <p class="text-bold">Genesis</p>
                    <span>art</span>,<span>illustration</span>
                </div>

                <div class="extra-info portfolio-image">
                    <a href="#" class="entry-img-hover-loupe"></a>
                    <a href="#" class="entry-img-hover-link"></a>
                </div>
            </div>
        </li>

        <li>
            <div class="portfolio-element image-hover art">
                <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/intrinsi.jpg" alt="intrinsi">
                
                <div class="portfolio-info">
                    <p class="text-bold">Genesis</p>
                    <span>art</span>,<span>illustration</span>
                </div>

                <div class="extra-info portfolio-image">
                    <a href="#" class="entry-img-hover-loupe"></a>
                    <a href="#" class="entry-img-hover-link"></a>
                </div>
            </div>
        </li>

        <li>
            <div class="portfolio-element image-hover art">
                <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/wonderland.jpg" alt="wonderland">
                
                <div class="portfolio-info">
                    <p class="text-bold">Genesis</p>
                    <span>art</span>,<span>illustration</span>
                </div>

                <div class="extra-info portfolio-image">
                    <a href="#" class="entry-img-hover-loupe"></a>
                    <a href="#" class="entry-img-hover-link"></a>
                </div>
            </div>
        </li>

        <li>
            <div class="portfolio-element image-hover art">
                <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/nanotech.jpg" alt="nanotech">
                
                <div class="portfolio-info">
                    <p class="text-bold">Genesis</p>
                    <span>art</span>,<span>illustration</span>
                </div>

                <div class="extra-info portfolio-image">
                    <a href="#" class="entry-img-hover-loupe"></a>
                    <a href="#" class="entry-img-hover-link"></a>
                </div>
            </div>
        </li>

        <li>
            <div class="portfolio-element image-hover art">
                <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/wonderland.jpg" alt="wonderland">
                
                <div class="portfolio-info">
                    <p class="text-bold">Genesis</p>
                    <span>art</span>,<span>illustration</span>
                </div>

                <div class="extra-info portfolio-image">
                    <a href="#" class="entry-img-hover-loupe"></a>
                    <a href="#" class="entry-img-hover-link"></a>
                </div>
            </div>
        </li>

        <li>
            <div class="portfolio-element image-hover art">
                <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/intrinsi.jpg" alt="intrinsi">
                
                <div class="portfolio-info">
                    <p class="text-bold">Genesis</p>
                    <span>art</span>,<span>illustration</span>
                </div>

                <div class="extra-info portfolio-image">
                    <a href="#" class="entry-img-hover-loupe"></a>
                    <a href="#" class="entry-img-hover-link"></a>
                </div>
            </div>
        </li>

        <li>
            <div class="portfolio-element image-hover art">
                <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/nanotech.jpg" alt="nanotech">
                
                <div class="portfolio-info">
                    <p class="text-bold">Genesis</p>
                    <span>art</span>,<span>illustration</span>
                </div>

                <div class="extra-info portfolio-image">
                    <a href="#" class="entry-img-hover-loupe"></a>
                    <a href="#" class="entry-img-hover-link"></a>
                </div>
            </div>
        </li>

        <li>
            <div class="portfolio-element image-hover art">
                <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/intrinsi.jpg" alt="intrinsi">
                
                <div class="portfolio-info">
                    <p class="text-bold">Genesis</p>
                    <span>art</span>,<span>illustration</span>
                </div>

                <div class="extra-info portfolio-image">
                    <a href="#" class="entry-img-hover-loupe"></a>
                    <a href="#" class="entry-img-hover-link"></a>
                </div>
            </div>
        </li>

      </ul>             
</section>