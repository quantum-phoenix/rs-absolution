<?php include 'header.php'; ?>

<section class="title-large">
    <div class="title-large-inner">
        <div class="container clr">
            <div class="row span_12 no-margin-col">

                <div class="col span_6">
                    <h2>Our <span class="text-bold"> Portfolio</span></h2>                    
                </div>

                <div class="col span_6">
                    <ul class="inline-ul breadcrumb">
                        <li><a href="#">Portfolio</a></li>
                        <li><a href="single_portfolio.php">Single portfolio</a></li>                        
                    </ul>
                </div>

            </div>
        </div>      
    </div>            
</section>

<section class="container clr">

    <div class="row row-big-col">
        <div class="col span_8">
			<section class="slider">
	            <div class="flexslider about-us-slider">
	              <ul class="slides">
	                <li>
	                    <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/theabyss.jpg" alt="theabyss">
	                </li>
	                <li>
	                    <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/forthepeople.jpg" alt="forthepeople">
	                </li>
	              </ul>
	            </div>
	        </section>
		</div>

		<div class="col span_4">

			<div class="single-portfolio-info">
				<div class="title-medium clr">
	                <h3>Project Details</h3>
	            </div>
			
				<div class="single-portfolio-list">
					<ul>
						<li class="clr">							
							<p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est illo recusandae placeat labore consequuntur perspiciatis maxime quisquam asperiores fuga officiis.Est illo recusandae placeat labore consequuntur perspiciatis maxime quisquam asperiores fuga officiis.</p>	
						</li>
						<li>
							<ul class="margin-bottom">							
								<li><span class="text-bold">Client:</span><span class="">Envato</span></li>
								<li><span class="text-bold">Date:</span><span class="">January, 2013</span></li>
								<li>
									<ul class="inline-ul-nm no-margin ul-hover-a">
										<li><span class="text-bold">Tags:</span></li>
                                        <li><a href="single_blog.php">art,</a></li>
                                        <li><a href="single_blog.php">illustration,</a></li>
                                        <li><a href="single_blog.php">harmony</a></li>
                                    </ul>									
								</li>
							</ul>
						</li>						
						<li>
							<a class="button-a" href="#">
								<span class="button green small">Launch Project</span>
							</a>	
						</li>
					</ul>
				</div>

			</div>

		</div>

    </div>

	<div class="row">
        <div class="col span_12">
            <div class="title-medium">
                <h3>Recent Portfolio</h3>
            </div>
        </div>
    </div>

	<div class="row">
	    <div class="col span_12">
	    	<?php include 'single_portfolio_slider.php'; ?>
	    </div>
	</div>

</section>
<?php include 'footer.php'; ?>