<?php include 'header.php'; ?>
<?php include 'revolution.php'; ?>
<section class="container clr">

    <div class="row row-big-col">
        <div class="col span_12">
            <div class="presentation-box">
                <img src="css/images/medium_content_box1.png" alt="medium_content_box1">
                <h4 class="text-align-center text-bold">Responsive Layout</h4>
                <p class="text-align-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex esse!</p>
            </div>     
        
            <div class="presentation-box">
                <img src="css/images/medium_content_box2.png" alt="medium_content_box2">
                <h4 class="text-align-center text-bold">Clean Design</h4>
                <p class="text-align-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur placeat.</p>
            </div>       

            <div class="presentation-box">
                <img src="css/images/medium_content_box3.png" alt="medium_content_box3">
                <h4 class="text-align-center text-bold">Great Support</h4>
                <p class="text-align-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore laudantium.</p>
            </div>
        
            <div class="presentation-box">
                <img src="css/images/medium_content_box4.png" alt="medium_content_box4">
                <h4 class="text-align-center text-bold">Good Features</h4>
                <p class="text-align-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste nulla.</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col span_12">
            <div class="title-medium">
                <h3>Recent Portfolio</h3>
            </div>
        </div>
    </div>

    <div class="row row-big-col">
        <div class="col span_12">
            <?php include 'single_portfolio_slider.php'; ?>
        </div>
    </div>

    <div class="row">
        <div class="col span_12">
            <div class="title-medium">
                <h3>Latest post from our Blog & Services</h3>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col span_3">
            <article class="entry home-entry">
                <div class="home-entry-meta">
                    <div class="meta-date">
                        <p>15</p>
                        <p>FEB</p>
                    </div>
                    <div class="meta-ico"></div>
                </div>
                <div class="entry-image">
                    <div class="image-hover entry-image-blog">
                         <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/blog/africa.jpg" alt="africa">                                      
                    </div>
                    <div class="home-image-shadow"></div>                    
                </div>
                <div class="entry-text">
                    <a href="#">
                        <h3 class="content-green-hover no-margin text-bold">Lorem ipsum dolor sit amet, consectetur.</h3>
                    </a>
                    <ul class="inline-ul">
                        <li>
                            <span class="gray-text">Posted by: </span>
                            <span class="gray-text">Admin</span>
                        </li>
                        <li>
                            <span class="gray-text">Comments: </span>
                            <span class="gray-text">3</span>
                        </li>                        
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam quo accusamus quidem incidunt animi fuga laudantium natus vero.</p>
                    <a href="single_blog.php">
                        <span class="content-green-hover read-more">Read More</span>
                    </a>
                </div>
            </article>
        </div>
        <div class="col span_3">
            <article class="entry home-entry">
                <div class="home-entry-meta">
                    <div class="meta-date">
                        <p>15</p>
                        <p>FEB</p>
                    </div>
                    <div class="meta-ico"></div>
                </div>
                <div class="entry-image">
                    <div class="image-hover entry-image-blog">
                         <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/blog/redpassion.jpg" alt="redpassion">                                       
                    </div>
                    <div class="home-image-shadow"></div>                    
                </div>
                <div class="entry-text">
                    <a href="#">
                        <h3 class="content-green-hover no-margin text-bold">Lorem ipsum dolor sit amet, consectetur.</h3>
                    </a>
                    <ul class="inline-ul">
                        <li>
                            <span class="gray-text">Posted by: </span>
                            <span class="gray-text">Admin</span>
                        </li>
                        <li>
                            <span class="gray-text">Comments: </span>
                            <span class="gray-text">3</span>
                        </li>                        
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam quo accusamus quidem incidunt animi fuga laudantium natus vero.</p>
                    <a href="single_blog.php">
                        <span class="content-green-hover read-more">Read More</span>
                    </a>
                </div>
            </article>
        </div>
        <div class="col span_6">
            <div class="accordion clr">
                <div class="title">More Questions?</div>
                <div class="inner">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dictum viverra enim vitae malesuada. Cras nec odio urna, sed posuere neque. Donec scelerisque, diam non placerat tempus, ligula sem vehicula.</p>
                </div>
            </div>

            <div class="accordion clr">
                <div class="title">More Questions?</div>
                <div class="inner">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dictum viverra enim vitae malesuada. Cras nec odio urna, sed posuere neque. Donec scelerisque, diam non placerat tempus, ligula sem vehicula.</p>
                </div>
            </div>

            <div class="accordion clr">
                <div class="title">More Questions?</div>
                <div class="inner">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dictum viverra enim vitae malesuada. Cras nec odio urna, sed posuere neque. Donec scelerisque, diam non placerat tempus, ligula sem vehicula.</p>
                </div>
            </div>

            <div class="accordion clr">
                <div class="title">More Questions?</div>
                <div class="inner">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dictum viverra enim vitae malesuada. Cras nec odio urna, sed posuere neque. Donec scelerisque, diam non placerat tempus, ligula sem vehicula.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col span_12">
            <div class="title-medium">
                <h3>Our Clients</h3>                
            </div>
        </div>
    </div>

    <div class="row row-big-col">
        <div class="col span_12">
            <?php include 'clients.php'; ?>
        </div>
    </div>

</section>
<?php include 'footer.php'; ?>