<?php include 'header.php'; ?>

<section class="title-large">
    <div class="title-large-inner">
        <div class="container clr">
            <div class="row span_12 no-margin-col">

                <div class="col span_6">
                    <h2>Welcome to our<span class="text-bold"> Blog</span></h2>                    
                </div>

                <div class="col span_6">
                    <ul class="inline-ul breadcrumb">
                        <li><a href="index.php">Blog</a></li>                        
                    </ul>
                </div>

            </div>
        </div>      
    </div>            
</section>

<section class="container clr">
    <div class="row">

        <div class="col span_8">
            <article class="entry">
                <div class="entry-meta">
                    <div class="meta-date">
                        <p>15</p>
                        <p>FEB</p>
                    </div>
                    <div class="meta-ico"></div>
                </div>
                <div class="entry-image">
                    <div class="image-hover blog-image">
                         <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/blog/redpassion_large.jpg" alt="redpassion_large">
                         <div class="extra-info blog-image">
                            <a href="#" class="entry-img-hover-loupe"></a>
                         </div>                
                    </div>
                    <div class="image-shadow"></div>                    
                </div>
                <div class="entry-text">
                    <a href="#">
                        <h3 class="content-green-hover no-margin text-bold">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et ullam?</h3>
                    </a>
                    <ul class="inline-ul entry-text-ul">
                        <li>
                            <span class="gray-text">Posted by: </span>
                            <span class="gray-text">Admin</span>
                        </li>
                        <li>
                            <span class="gray-text">Comments: </span>
                            <span class="gray-text"><a href="single_blog.php">3</a></span>
                        </li>
                        <li>
                            <ul class="inline-ul no-margin">
                                <li><span class="gray-text">Taged by: </span></li>
                                <li>
                                   <ul class="inline-ul no-margin">
                                        <li><a href="single_blog.php">art,</a></li>
                                        <li><a href="single_blog.php">illustration,</a></li>
                                        <li><a href="single_blog.php">harmony</a></li>
                                    </ul> 
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam quo accusamus quidem incidunt animi fuga laudantium natus vero cumque eius accusantium ratione molestiae facilis modi ipsum maiores libero distinctio debitis neque maxime. Aut reiciendis voluptates aspernatur consequuntur modi delectus sunt voluptatem est a voluptatum error tempore necessitatibus fugiat perspiciatis ratione hic dolores quas itaque ut iusto atque perferendis placeat molestiae!</p>
                    <a href="single_blog.php">
                        <span class="content-green-hover read-more">Read More</span>
                    </a>
                </div>
            </article>

            <div class="divider-large"></div>

            <article class="entry">
                <div class="entry-meta">
                    <div class="meta-date">
                        <p>15</p>
                        <p>FEB</p>
                    </div>
                    <div class="meta-ico"></div>
                </div>
                <div class="entry-image">
                    <div class="image-hover blog-image">
                         <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/blog/natural_freedom_large.jpg" alt="natural_freedom_large">
                         <div class="extra-info blog-image">
                            <a href="#" class="entry-img-hover-loupe"></a>
                         </div>                
                    </div>
                    <div class="image-shadow"></div>                    
                </div>
                <div class="entry-text">
                    <a href="#">
                        <h3 class="content-green-hover no-margin text-bold">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et ullam?</h3>
                    </a>
                    <ul class="inline-ul entry-text-ul">
                        <li>
                            <span class="gray-text">Posted by: </span>
                            <span class="gray-text">Admin</span>
                        </li>
                        <li>
                            <span class="gray-text">Comments: </span>
                            <span class="gray-text"><a href="single_blog.php">3</a></span>
                        </li>
                        <li>
                            <ul class="inline-ul no-margin">
                                <li><span class="gray-text">Taged by: </span></li>
                                <li>
                                   <ul class="inline-ul no-margin">
                                        <li><a href="single_blog.php">art,</a></li>
                                        <li><a href="single_blog.php">illustration,</a></li>
                                        <li><a href="single_blog.php">harmony</a></li>
                                    </ul> 
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam quo accusamus quidem incidunt animi fuga laudantium natus vero cumque eius accusantium ratione molestiae facilis modi ipsum maiores libero distinctio debitis neque maxime. Aut reiciendis voluptates aspernatur consequuntur modi delectus sunt voluptatem est a voluptatum error tempore necessitatibus fugiat perspiciatis ratione hic dolores quas itaque ut iusto atque perferendis placeat molestiae!</p>
                    <a href="single_blog.php">
                        <span class="content-green-hover read-more">Read More</span>
                    </a>
                </div>
            </article>

            <div class="divider-large"></div>

            <article class="entry">
                <div class="entry-meta">
                    <div class="meta-date">
                        <p>15</p>
                        <p>FEB</p>
                    </div>
                    <div class="meta-ico"></div>
                </div>
                <div class="entry-image">
                    <div class="image-hover blog-image">
                         <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/blog/africa_large.jpg" alt="africa_large">
                         <div class="extra-info blog-image">
                            <a href="#" class="entry-img-hover-loupe"></a>
                         </div>                
                    </div>
                    <div class="image-shadow"></div>                    
                </div>
                <div class="entry-text">
                    <a href="#">
                        <h3 class="content-green-hover no-margin text-bold">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et ullam?</h3>
                    </a>
                    <ul class="inline-ul entry-text-ul">
                        <li>
                            <span class="gray-text">Posted by: </span>
                            <span class="gray-text">Admin</span>
                        </li>
                        <li>
                            <span class="gray-text">Comments: </span>
                            <span class="gray-text"><a href="single_blog.php">3</a></span>
                        </li>
                        <li>
                            <ul class="inline-ul no-margin">
                                <li><span class="gray-text">Taged by: </span></li>
                                <li>
                                   <ul class="inline-ul no-margin">
                                        <li><a href="single_blog.php">art,</a></li>
                                        <li><a href="single_blog.php">illustration,</a></li>
                                        <li><a href="single_blog.php">harmony</a></li>
                                    </ul> 
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam quo accusamus quidem incidunt animi fuga laudantium natus vero cumque eius accusantium ratione molestiae facilis modi ipsum maiores libero distinctio debitis neque maxime. Aut reiciendis voluptates aspernatur consequuntur modi delectus sunt voluptatem est a voluptatum error tempore necessitatibus fugiat perspiciatis ratione hic dolores quas itaque ut iusto atque perferendis placeat molestiae!</p>
                    <a href="single_blog.php">
                        <span class="content-green-hover read-more">Read More</span>
                    </a>
                </div>
            </article>

            <div class="pager clr">
                <a href="#" class="back-pager"></a>
                <ul>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                </ul>
                <a href="#" class="next-pager"></a>
            </div>

        </div>

        <div class="col span_4">
            <sidebar class="widget-area row span_12">

                <div class="widget widget_search">
                    <input type="text" name="search" value="Search here...">
                </div>

                <div class="widget widget_recent_entries">
                        <div class="title-small clr">
                            <h5>Categories</h5>
                        </div>
                    <ul>
                        <li><a href="#">Ut a diam id enim sodales vestibulum</a></li>
                        <li><a href="#">Aliquam posuere, augue ut</a></li>
                        <li><a href="#">Vestibulum cursus elit placerat sapien</a></li>
                        <li><a href="#">Pellentesque eu lobortis dolor</a></li>
                        <li><a href="#">Etiam gravida aliquet metus</a></li>
                    </ul>
                </div>

                <div class="widget">
                    <div class="title-small clr">
                        <h5>Accordion</h5>
                    </div>
                    <div class="accordion clr">
                        <div class="title">More Questions?</div>
                        <div class="inner">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dictum viverra enim vitae malesuada. Cras nec odio urna, sed posuere neque. Donec scelerisque, diam non placerat tempus, ligula sem vehicula.</p>
                        </div>
                    </div>

                    <div class="accordion clr">
                        <div class="title">More Questions?</div>
                        <div class="inner">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dictum viverra enim vitae malesuada. Cras nec odio urna, sed posuere neque. Donec scelerisque, diam non placerat tempus, ligula sem vehicula.</p>
                        </div>
                    </div>

                    <div class="accordion clr">
                        <div class="title">More Questions?</div>
                        <div class="inner">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dictum viverra enim vitae malesuada. Cras nec odio urna, sed posuere neque. Donec scelerisque, diam non placerat tempus, ligula sem vehicula.</p>
                        </div>
                    </div>

                    <div class="accordion clr">
                        <div class="title">More Questions?</div>
                        <div class="inner">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dictum viverra enim vitae malesuada. Cras nec odio urna, sed posuere neque. Donec scelerisque, diam non placerat tempus, ligula sem vehicula.</p>
                        </div>
                    </div>
                </div>

                <div class="widget recent-comments-widget">
                        <div class="title-small clr">
                            <h5>Recent <span class="text-bold">Comments</span></h5>
                        </div>
                    <ul>
                        <li><span>admin on</span><span> <a href="#" class="content-green-hover">Sample post 1</a></span></li>
                        <li><span>admin on</span><span> <a href="#" class="content-green-hover">Sample post 2</a></span></li>
                        <li><span>admin on</span><span> <a href="#" class="content-green-hover">Sample post 3</a></span></li>
                        <li><span>admin on</span><span> <a href="#" class="content-green-hover">Sample post 4</a></span></li>
                        <li><span>admin on</span><span> <a href="#" class="content-green-hover">Sample post 5</a></span></li>
                        <li><span>admin on</span><span> <a href="#" class="content-green-hover">Sample post 6</a></span></li>
                    </ul>
                </div>

                <div class="widget portfolio-widget">
                    <div class="title-small clr">
                        <h5>Recent <span class="text-bold">Portfolio</span></h5>
                    </div>
                    <section class="slider">
                        <div class="flexslider single-portfolio-slider single-portfolio-slider-widget">
                          <ul class="slides">
                            <li>
                                <div class="portfolio-element image-hover art">
                                    <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/genesis.jpg" alt="genesis">
                                    
                                    <div class="portfolio-info">
                                        <p>Genesis</p>
                                        <span>art</span>,<span>illustration</span>
                                    </div>

                                    <div class="extra-info portfolio-element-widget">
                                        <a href="#" class="entry-img-hover-loupe"></a>
                                        <a href="#" class="entry-img-hover-link"></a>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="portfolio-element image-hover art">
                                    <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/portfolio/intrinsi.jpg" alt="intrinsi">
                                    
                                    <div class="portfolio-info">
                                        <p>Genesis</p>
                                        <span>art</span>,<span>illustration</span>
                                    </div>

                                    <div class="extra-info portfolio-element-widget">
                                        <a href="#" class="entry-img-hover-loupe"></a>
                                        <a href="#" class="entry-img-hover-link"></a>
                                    </div>
                                </div>
                            </li>

                          </ul>
                        </div>
                    </section>
                </div>

                <div class="widget tags-widget">
                        <div class="title-small clr">
                            <h5>Popular <span class="text-bold">Tags</span></h5>
                        </div>
                    <div class="tagcloud clr">
                        <div><a href="#">Web</a></div>
                        <div><a href="#">Competition</a></div>
                        <div><a href="#">Logo</a></div>
                        <div><a href="#">Css</a></div>
                        <div><a href="#">WordPress</a></div>
                        <div><a href="#">WebDesign</a></div>
                    </div>
                </div>

                <div class="widget text-widget">
                        <div class="title-small clr">
                            <h5>Text <span class="text-bold">Widget</span></h5>
                        </div>                      
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis commodi sequi eligendi nisi harum natus iure vero fugiat ab quam!</p>
                </div>
                
            </sidebar>
        </div>
    </div>
</section>


<?php include 'footer.php'; ?>