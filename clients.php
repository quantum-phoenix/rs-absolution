<section class="slider jcarousel-clients-skin">
    <ul class="jcarousel-skin-absolution jcarousel-clients">
        <li>
            <div class="client">
                <div class="themeforest"></div>
            </div>
        </li>
        <li>
            <div class="client">
                <div class="graphicriver"></div>
            </div>
        </li>
        <li>
            <div class="client">
                <div class="codecanyon"></div>
            </div>
        </li>
        <li>
            <div class="client">
                <div class="audiojungle"></div>
            </div>
        </li>
        <li>
            <div class="client">
                <div class="activeden"></div>
            </div>
        </li>
        <!-- Next slide -->
        <li>
           <div class="client">
                <div class="activeden"></div>
            </div>
        </li>
        <li>
            <div class="client">
                <div class="audiojungle"></div>
            </div>
        </li>
        <li>
            <div class="client">
                <div class="codecanyon"></div>
            </div>
        </li>
        <li>
            <div class="client">
                <div class="graphicriver"></div>
            </div>
        </li>
        <li>
            <div class="client">
                <div class="themeforest"></div>
            </div>
        </li>
    </ul>
</section>