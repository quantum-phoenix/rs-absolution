$(document).ready(function () {
    /* =====================================================       
     Shortcodes     
     ===================================================== */

	/* -----------------------------------------------------
     Progress bar (About us)
     ----------------------------------------------------- */
    $(".meter > span").each(function () {
        $(this)
            .data("origWidth", $(this).width())
            .width(0)
            .animate({
                width:$(this).data("origWidth")
            }, 1200);
    });

    /* -----------------------------------------------------
     Alerts
     ----------------------------------------------------- */
    $(".alert-message a").click(function () {
        $(this).parent().slideUp();
        return false;
    });

    /* =====================================================
     Back to top
     ===================================================== */
    $(function() {
        $(window).scroll(function() {
            if($(this).scrollTop() != 0) {
                $('.to-top').fadeIn();
            } else {
                $('.to-top').fadeOut();
            }
        });

        $('.to-top').click(function() {
            $('body,html').animate({scrollTop:0},800);
        });
    });

    /* =====================================================       
     Contact     
     ===================================================== */
    $("#map").gMap({markers:[
        {
            latitude:47.660937,
            longitude:9.569803
        }
    ],
        zoom:10
    });
    /* -----------------------------------------------------
     Tabs
     ----------------------------------------------------- */
    var tabContainers = $('div.tabs > div');

    $('div.tabs ul.tab_navigation a').click(
        function () {
            tabContainers.hide();
            tabContainers.filter(this.hash).show();
            $('div.tabs ul.tab_navigation a').removeClass('active');
            $(this).addClass('active');
            return false;
        }).filter(':first').click();
    /* -----------------------------------------------------
     Accordion
     ----------------------------------------------------- */
    $(".accordion .title").click(function () {
        if($(this).hasClass('active')){        
            $(this).removeClass("active").closest($('.accordion')).find('.inner').slideUp(400, 'easeOutCirc', function () {
                $(this).next('.inner').css("display", "none");
            });               
        }
        else{
            $(this).addClass("active").closest($('.accordion')).find('.inner').slideDown(400, 'easeOutBounce', function () {
                $(this).next('.inner').css("display", "block");
            });            
        }
    });
    /* -----------------------------------------------------
     Testimonials
     ----------------------------------------------------- */
    $('.testimonials').flexslider({
        animation: "slide",
        controlNav: false,
        manualControls: ".flex-control-nav_ li",
    });
    /* -----------------------------------------------------
     About us slider
     ----------------------------------------------------- */
    $('.about-us-slider').flexslider({
        animation: "slide",
        controlNav: false,
        manualControls: ".flex-control-nav_ li",
    });
    /* -----------------------------------------------------
     Client slider
     ----------------------------------------------------- */
    (function () {
        var $carousel = $('.jcarousel-clients');
        if ($carousel.length) {
            var scrollCount = 5;

            function getWindowWidth() {
                if ($(window).width() < 480) {
                    scrollCount = 1;
                } else if ($(window).width() < 768) {
                    scrollCount = 2;
                } else if ($(window).width() < 960) {
                    scrollCount = 3;
                } else {
                    scrollCount = 5;
                }
            }

            function initCarousel(carousels) {
                carousels.each(function () {
                    console.log(scrollCount);
                    var $this = $(this);
                    $this.jcarousel({
                        animation:600,
                        easing:'easeOutCubic',
                        scroll:scrollCount,
                        itemVisibleInCallback:function () {
                            onBeforeAnimation : resetPosition($this);
                            onAfterAnimation : resetPosition($this);
                        },
                        auto:($this.data('auto') ? parseInt($this.data('auto')) : 0),
                        wrap:($this.data('auto') ? 'both' : null)
                    });
                });
            }

            function adjustCarousel() {
                $carousel.each(function () {
                    var $this = $(this),
                        $lis = $this.children('li')
                    newWidth = $lis.length * $lis.first().outerWidth(true) + 100;
                    console.log(newWidth);
                    getWindowWidth();
                    // Resize only if width has changed
                    if ($this.width() !== newWidth) {
                        $this.css('width', newWidth)
                            .data('resize', 'true');
                        initCarousel($this);
                        $this.jcarousel('scroll', 1);
                        var timer = window.setTimeout(function () {
                            window.clearTimeout(timer);
                            $this.data('resize', null);
                        }, 600);
                    }
                });
            }

            function resetPosition(elem, resizeEvent) {
                if (elem.data('resize'))
                    elem.css('left', '0');
            }

            getWindowWidth();
            initCarousel($carousel);

            // Detect swipe gestures support
            if (Modernizr.touch) {
                function swipeFunc(e, dir) {
                    var $carousel = $(e.currentTarget);
                    if (dir === 'left')
                        $carousel.parent('.jcarousel-clip').siblings('.jcarousel-next').trigger('click');
                    if (dir === 'right')
                        $carousel.parent('.jcarousel-clip').siblings('.jcarousel-prev').trigger('click');
                }

                $carousel.swipe({
                    swipeLeft:swipeFunc,
                    swipeRight:swipeFunc,
                    allowPageScroll:'auto'
                });

            }

            // Window resize
            $(window).on('resize', function () {
                var timer = window.setTimeout(function () {
                    window.clearTimeout(timer);
                    adjustCarousel();
                }, 30);
            });

        }

    })();
    /* -----------------------------------------------------
     Single portfolio slider
     ----------------------------------------------------- */
    (function () {
        var $carousel = $('.jcarousel-single-portfolio');
        if ($carousel.length) {
            var scrollCount = 4;

            function getWindowWidth() {
                if ($(window).width() < 480) {
                    scrollCount = 1;
                } else if ($(window).width() < 768) {
                    scrollCount = 2;
                } else if ($(window).width() < 960) {
                    scrollCount = 3;
                } else {
                    scrollCount = 4;
                }
            }

            function initCarousel(carousels) {
                carousels.each(function () {
                    console.log(scrollCount);
                    var $this = $(this);
                    $this.jcarousel({
                        animation:600,
                        easing:'easeOutCubic',
                        scroll:scrollCount,
                        itemVisibleInCallback:function () {
                            onBeforeAnimation : resetPosition($this);
                            onAfterAnimation : resetPosition($this);
                        },
                        auto:($this.data('auto') ? parseInt($this.data('auto')) : 0),
                        wrap:($this.data('auto') ? 'both' : null)
                    });
                });
            }

            function adjustCarousel() {
                $carousel.each(function () {
                    var $this = $(this),
                        $lis = $this.children('li')
                    newWidth = $lis.length * $lis.first().outerWidth(true) + 100;
                    console.log(newWidth);
                    getWindowWidth();
                    // Resize only if width has changed
                    if ($this.width() !== newWidth) {
                        $this.css('width', newWidth)
                            .data('resize', 'true');
                        initCarousel($this);
                        $this.jcarousel('scroll', 1);
                        var timer = window.setTimeout(function () {
                            window.clearTimeout(timer);
                            $this.data('resize', null);
                        }, 600);
                    }
                });
            }

            function resetPosition(elem, resizeEvent) {
                if (elem.data('resize'))
                    elem.css('left', '0');
            }

            getWindowWidth();
            initCarousel($carousel);

            // Detect swipe gestures support
            if (Modernizr.touch) {
                function swipeFunc(e, dir) {
                    var $carousel = $(e.currentTarget);
                    if (dir === 'left')
                        $carousel.parent('.jcarousel-clip').siblings('.jcarousel-next').trigger('click');
                    if (dir === 'right')
                        $carousel.parent('.jcarousel-clip').siblings('.jcarousel-prev').trigger('click');
                }

                $carousel.swipe({
                    swipeLeft:swipeFunc,
                    swipeRight:swipeFunc,
                    allowPageScroll:'auto'
                });

            }

            // Window resize
            $(window).on('resize', function () {
                var timer = window.setTimeout(function () {
                    window.clearTimeout(timer);
                    adjustCarousel();
                }, 30);
            });

        }

    })();
    /* -----------------------------------------------------
     Single portfolio slider
     ----------------------------------------------------- */
    $('.single-portfolio-slider').flexslider({
        animation: "slide",
        controlNav: false,
        manualControls: ".flex-control-nav_ li",
    }); 
    /* =====================================================       
     Footer    
     ===================================================== */   
     $(".tweet").tweet({
        username:"jeb_",
        join_text:"auto",
        avatar_size:40,
        count:3,
        auto_join_text_default:"we said,",
        auto_join_text_ed:"we",
        auto_join_text_ing:"we were",
        auto_join_text_reply:"we replied to",
        auto_join_text_url:"we were checking out",
        loading_text:"loading tweets..."
    });
    /* =====================================================       
     Isotope     
     ===================================================== */
    var $container = $('#portfolio');
        $container.isotope({
            transformsEnabled:true,
            filter:'.portfolio-element',
            getSortData:{
                number:function ($elem) {
                    return parseFloat($elem.find('.player_numbers').text().replace(/[\(\)]/g, ''));
                }
            },
            sortBy:'number',
            sortAscending:false
        });
        
        var $optionSets = $('.option-set'),
            $optionLinks = $optionSets.find('a');
        $optionLinks.click(function () {
            var $this = $(this);
            var $optionSet = $this.parents('.option-set');
            if ($this.hasClass('selected'))
                return false;
            $optionSet.find('.selected').removeClass('selected');
            $this.addClass('selected');
            var filter = '.' + $this.attr("id").toLowerCase();
            if (filter == '.all')
                filter = '.portfolio-element';
            $container.isotope({
                transformsEnabled:true,
                filter:filter,
                getSortData:{
                    number:function ($elem) {
                        return parseFloat($elem.find('.player_numbers').text().replace(/[\(\)]/g, ''));
                    }
                },
                sortBy:'number',
                sortAscending:false
            });
            return false;
        });
    /* =====================================================
     Mobil menu
     ===================================================== */
    $('.menu-system ul.absolution').mobileMenu({
        defaultText:'Navigate to...',
        className:'select-menu',
        subMenuDash:'&nbsp;&nbsp;&nbsp;&ndash;'
    });


    /* =====================================================       
     Home     
     ===================================================== */
    /* '$.fn.cssOriginal!=undefined' */
    if (true){

        /*$.fn.css = $.fn.cssOriginal;*/

        $('.fullwidthbanner').revolution(
            {
            delay:9000,
            startwidth:960,
            startheight:500,

            onHoverStop:"on",                       // Stop Banner Timet at Hover on Slide on/off

            thumbWidth:100,                         // Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
            thumbHeight:50,
            thumbAmount:3,

            hideThumbs:0,
            navigationType:"none",                // bullet, thumb, none
            navigationArrows:"solo",                // nexttobullets, solo (old name verticalcentered), none

            navigationStyle:"round",                // round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom


            navigationHAlign:"center",              // Vertical Align top,center,bottom
            navigationVAlign:"top",                 // Horizontal Align left,center,right
            navigationHOffset:0,
            navigationVOffset:20,

            soloArrowLeftHalign:"left",
            soloArrowLeftValign:"center",
            soloArrowLeftHOffset:0,
            soloArrowLeftVOffset:0,

            soloArrowRightHalign:"right",
            soloArrowRightValign:"center",
            soloArrowRightHOffset:0,
            soloArrowRightVOffset:0,

            touchenabled:"on",                      // Enable Swipe Function : on/off



            stopAtSlide:-1,                         // Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
            stopAfterLoops:-1,                      // Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic

            hideCaptionAtLimit:0,                   // It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
            hideAllCaptionAtLilmit:0,               // Hide all The Captions if Width of Browser is less then this value
            hideSliderAtLimit:0,                    // Hide the whole slider, and stop also functions if Width of Browser is less than this value


            fullWidth:"on",

            shadow:0                                //0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)
        });
    }

});