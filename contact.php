<?php include 'header.php'; ?>

<section class="title-large no-margin">
    <div class="title-large-inner">
        <div class="container clr">
            <div class="row span_12 no-margin-col">

                <div class="col span_6">
                    <h2>Contact <span class="text-bold"> Us</span></h2>                    
                </div>

                <div class="col span_6">
                    <ul class="inline-ul breadcrumb">
                        <li><a href="contact.php">Contact</a></li>                        
                    </ul>
                </div>

            </div>
        </div>      
    </div>            
</section>

<div id="map"></div>
<section class="container clr">
    <div class="row">
        <div class="col span_12">
            <h3 class="text-center no-margin tif-text">We are a team of <span class="contact-green">Rubidium Style</span> , we love the minimalist and clean responsive WordPress and HTML themes.</h3>
            <h3 class="text-center tif-text">We set new standards in user experience & make future happen.</h3>
        </div>
    </div>

    <div class="row">
        <div class="col span_8">
        	<div class="title-medium">
                <h3>Leave a Reply</h3>
            </div>

            <form action="#">
                <div class="row">

                    <div class="col span_4">                        
                        <input class="default-input name" type="text" name="name" value="Name">
                    </div>
                    <div class="col span_4">                        
                        <input class="default-input email" type="text" name="email" value="Email">
                    </div>
                    <div class="col span_4">                        
                        <input class="default-input website" type="text" name="website" value="Website">
                    </div>

                </div>
                <div class="row">
                    <div class="col span_12">
                    	<ul>                        
                        <li><textarea class="default-input pen" rows="4" cols="50">Your message</textarea></li>
                    	<ul>
                    </div>
                </div>
				<a class="button-a" href="#">
					<span class="button green small">Small</span>
				</a>	
            </form>
        </div>

        <div class="col span_4">
        	<div class="title-medium">
                <h3>Informations</h3>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis similique culpa maiores minima veniam dicta eius? Voluptatem expedita blanditiis labore.</p>
			<p class="no-margin">30 South Park Avenue</p>
            <p class="no-margin">San Francisco, CA 94108</p>
            <p>USA</p>
            <ul class="margin-bottom">
                <li><span class="text-bold">Phone: </span><span>(123) 456-7890</span></li>
                <li><span class="text-bold">Fax: </span><span>+08 (123) 456-7890</span></li>
                <li><span class="text-bold">Email: </span><span>contact@companyname.com</span></li>
                <li><span class="text-bold">Web: </span><span>companyname.com</span></li>
            </ul>     
           <div class="footer-social-icons small clr">
                <a href="#">
                    <div class="social-icon-dribbble"></div>
                </a>

                <a href="#">
                    <div class="social-icon-facebook"></div>
                </a>

                <a href="#">
                    <div class="social-icon-skype"></div>
                </a>

                <a href="#">
                    <div class="social-icon-twitter"></div>
                </a>

                <a href="#">
                    <div class="social-icon-vimeo"></div>
                </a>
            </div>
        </div>

    </div>

</section>

<?php include 'footer.php'; ?>