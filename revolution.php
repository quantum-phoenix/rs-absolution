<!--
#################################
- THEMEPUNCH BANNER -
#################################
-->
<div class="fullwidthbanner-container">
		<div class="fullwidthbanner">
			<ul>
				<!-- THE FIRST SLIDE -->
				<li data-transition="slotfadevertical" data-slotamount="10" data-masterspeed="5000" data-thumb="http://www.rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/home/natural-slider.jpg">
							<!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
							<img src="http://www.rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/home/natural-slider.jpg" data-fullwidthcentering="on">
							
							<div class="caption rev-green lfl stl"
								 data-x="255"
								 data-y="165"
								 data-speed="450"
								 data-start="800"
								 data-easing="easeInOutBack" data-end="8500" data-endspeed="300" data-endeasing="easeInSine" >WELCOME TO ABSOLUTION</div>
								 
							<div class="caption rev-green2 lfl stl"
								 data-x="242"
								 data-y="210"
								 data-speed="1100"
								 data-start="800"
								 data-easing="easeInOutElastic" data-end="9100" data-endspeed="150" data-endeasing="easeInSine" >MINIMALIST & CLEAN RESPONSIVE THEME</div>
								
						
				<!-- THE SECOND SLIDE -->
				<li data-transition="slideup" data-slotamount="15" data-masterspeed="5000" data-delay="9400" data-thumb="http://www.rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/home/pixel-city-slider.jpg">
							<img src="http://www.rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/home/pixel-city-slider.jpg" data-fullwidthcentering="on">

						<div class="caption rev-green lfl stl"
								 data-x="215"
								 data-y="165"
								 data-speed="500"
								 data-start="800"
								 data-easing="easeInQuad" data-end="9100" data-endspeed="300" data-endeasing="easeInSine" >REVOLUTION SLIDER INCLUDED</div>
								 
							<div class="caption rev-green2 lfl stl"
								 data-x="228"
								 data-y="210"
								 data-speed="1500"
								 data-start="800"
								 data-easing="easeInOutElastic" data-end="9100" data-endspeed="150" data-endeasing="easeInSine" >THIS SLIDER HAS A LOT OF GREAT FEATURES</div>
								 
								</li>
				<li data-transition="slideright" data-slotamount="1" data-masterspeed="300" data-thumb="http://www.rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/home/project-400-slider.jpg">
									<img src="http://www.rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/home/project-400-slider.jpg" data-fullwidthcentering="on">
									
							<div class="caption rev-green lfl stl"
								 data-x="220"
								 data-y="165"
								 data-speed="450"
								 data-start="800"
								 data-easing="easeInOutBack" data-end="8500" data-endspeed="300" data-endeasing="easeInSine" >ABSOLUTION LOOKS GOOD ON</div>
								 
							<div class="caption rev-green2 lfl stl"
								 data-x="380"
								 data-y="210"
								 data-speed="1100"
								 data-start="800"
								 data-easing="easeInOutElastic" data-end="9100" data-endspeed="150" data-endeasing="easeInSine" >ALL SCREEN SIZE</div>
				</li>

				</li>
			</ul>

			<div class="tp-bannertimer"></div>
		</div>
	</div>
<!--
##############################
 - ACTIVATE THE BANNER HERE -
##############################
-->