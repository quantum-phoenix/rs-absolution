<?php include 'header.php'; ?>

<section class="title-large">
    <div class="title-large-inner">
        <div class="container clr">
            <div class="row span_12 no-margin-col">

                <div class="col span_6">
                    <h2>Shortcodes</h2>                    
                </div>

                <div class="col span_6">
                    <ul class="inline-ul breadcrumb">
                        <li><a href="#">Pages</a></li>
                        <li><a href="shortcodes.php">Shortcodes</a></li>                        
                    </ul>
                </div>

            </div>
        </div>      
    </div>            
</section>

<section class="container clr">

    <div class="row">

        <div class="col span_12">
            <div class="title-medium">
                <h3>Alert Boxes & Testimonials</h3>
            </div>
        </div>

    </div>

    <div class="row row-big-col">

        <div class="col span_6">
	        <div class="alert-message info"><a class="close" href="#">×</a>
	            <p>Information Box.</p>
	        </div>

	        <div class="alert-message warning"><a class="close" href="#">×</a>
	            <p>Warning Box.</p>
	        </div>

	        <div class="alert-message success"><a class="close" href="#">×</a>
	            <p>Success Box.</p>
	        </div>

	        <div class="alert-message error"><a class="close" href="#">×</a>
	            <p>Error Box.</p>
	        </div>
        </div>

        <div class="col span_6">
			<section class="slider">
            <div class="flexslider testimonials">
              <ul class="slides">
                <li>
	                <div class="testimonials-author-div">
		                <p class="testimonials-author-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt laborum quis ipsum ratione doloribus. Fugit nulla laborum dolorem minima soluta.</p>
		                </div>
		                <div class="testimonials-arrow"></div>
		                <div class="green text-align-center">
		                <span class="testimonials-author">John Doe</span><span class="white-text"> / Web Design and Developer</span>
	                </div>
                </li>                
                <li>
	                <div class="testimonials-author-div">
		                <p class="testimonials-author-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt laborum quis ipsum ratione doloribus. Fugit nulla laborum dolorem minima soluta.</p>
		                </div>
		                <div class="testimonials-arrow"></div>
		                <div class="green text-align-center">
		                <span class="testimonials-author">John Doe</span><span class="white-text"> / Web Design and Developer</span>
	                </div>
                </li>
                <li>
	                <div class="testimonials-author-div">
		                <p class="testimonials-author-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt laborum quis ipsum ratione doloribus. Fugit nulla laborum dolorem minima soluta.</p>
		                </div>
		                <div class="testimonials-arrow"></div>
		                <div class="green text-align-center">
		                <span class="testimonials-author">John Doe</span><span class="white-text"> / Web Design and Developer</span>
	                </div>
                </li>
                <li>
	                <div class="testimonials-author-div">
		                <p class="testimonials-author-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt laborum quis ipsum ratione doloribus. Fugit nulla laborum dolorem minima soluta.</p>
		                </div>
		                <div class="testimonials-arrow"></div>
		                <div class="green text-align-center">
		                <span class="testimonials-author">John Doe</span><span class="white-text"> / Web Design and Developer</span>
	                </div>
                </li>
              </ul>
            </div>
          </section>
        </div>
    </div>

    <div class="row">
        <div class="col span_12">
            <div class="title-medium">
                <h3>Tabs & Accordions</h3>
            </div>
        </div>
    </div>

    <div class="row row-big-col">

        <div class="col span_6">
    		<div class="tabs">
                <ul class="tab_navigation clr">
                    <li><a href="#tab-1">First Tab</a></li>
                    <li><a href="#tab-2">Second Tab</a></li>
                    <li><a href="#tab-3">Third tab</a></li>
                </ul>
                <div id="tab-1">Mauris eu metus id lectus venenatis placerat. Nam mattis diam vitae lacus dictum quis dictum libero commodo. Etiam sagittis malesuada nisi eget vehicula. Nulla auctor mauris massa, et ornare odio. Mauris ultrices porta mauris ac suscipit. Cras congue elementum turpis, nec sodales turpis euismod sit amet. Phasellus vulputate semper nibh non hendrerit.</div>
                <div id="tab-2">Cras congue elementum turpis, nec sodales turpis euismod sit amet. Phasellus vulputate semper nibh non hendrerit.</div>
                <div id="tab-3">Mauris eu metus id lectus venenatis placerat. Cras congue elementum turpis, nec sodales turpis euismod sit amet. Phasellus vulputate semper nibh non hendrerit.</div>
            </div>
        </div>

        <div class="col span_6">
    		<div class="accordion clr">
                <div class="title">More Questions?</div>
                <div class="inner">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dictum viverra enim vitae malesuada. Cras nec odio urna, sed posuere neque. Donec scelerisque, diam non placerat tempus, ligula sem vehicula.</p>
                </div>
            </div>

            <div class="accordion clr">
                <div class="title">More Questions?</div>
                <div class="inner">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dictum viverra enim vitae malesuada. Cras nec odio urna, sed posuere neque. Donec scelerisque, diam non placerat tempus, ligula sem vehicula.</p>
                </div>
            </div>

            <div class="accordion clr">
                <div class="title">More Questions?</div>
                <div class="inner">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dictum viverra enim vitae malesuada. Cras nec odio urna, sed posuere neque. Donec scelerisque, diam non placerat tempus, ligula sem vehicula.</p>
                </div>
            </div>

            <div class="accordion clr">
                <div class="title">More Questions?</div>
                <div class="inner">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dictum viverra enim vitae malesuada. Cras nec odio urna, sed posuere neque. Donec scelerisque, diam non placerat tempus, ligula sem vehicula.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col span_12">
            <div class="title-medium">
                <h3>Buttons & Progress Bar</h3>
            </div>
        </div>
    </div>

    <div class="row row-big-col">
        <div class="col span_6">
        	<ul class="inline-ul">
        		<li>
		        	<a class="button-a" href="#">
						<span class="button green small">Small</span>
					</a>
        		</li>
        		<li>
        			<a class="button-a" href="#">
						<span class="button dark-gray small">Small</span>
					</a>
        		</li>
        		<li>
        			<a class="button-a" href="#">
						<span class="button light-gray small">Small</span>
					</a>
        		</li>
        	</ul>
        	<ul class="inline-ul">
        		<li>
		        	<a class="button-a" href="#">
						<span class="button green medium">Medium</span>
					</a>
        		</li>
        		<li>
        			<a class="button-a" href="#">
						<span class="button dark-gray medium">Medium</span>
					</a>
        		</li>
        		<li>
        			<a class="button-a" href="#">
						<span class="button light-gray medium">Medium</span>
					</a>
        		</li>
        	</ul>
        	<ul class="inline-ul">
        		<li>
		        	<a class="button-a" href="#">
						<span class="button green large">Large</span>
					</a>
        		</li>
        		<li>
        			<a class="button-a" href="#">
						<span class="button dark-gray large">Large</span>
					</a>
        		</li>
        		<li>
        			<a class="button-a" href="#">
						<span class="button light-gray large">Large</span>
					</a>
        		</li>
        	</ul>
		</div>

        <div class="col span_6">

			<div class="progress-bar margin-bottom">
		        <div class="meter">
		            <span style="width: 60%"></span>
		        </div>
		        <p class="margin-bottom-half">Web design: 60%</p>
			</div>

			<div class="progress-bar margin-bottom">
		        <div class="meter">
		            <span style="width: 80%"></span>
		        </div>
		        <p class="margin-bottom-half">WordPress: 80%</p>
			</div>

			<div class="progress-bar margin-bottom">
		        <div class="meter">
		            <span style="width: 40%"></span>
		        </div>
		        <p class="margin-bottom-half">Branding: 40%</p>
	        </div>

			<div class="progress-bar margin-bottom">
		        <div class="meter">
		            <span style="width: 90%"></span>
		        </div>
		        <p class="margin-bottom-half">Logo Design: 90%</p>
	        </div>

	        <div class="progress-bar margin-bottom">
		        <div class="meter">
		            <span style="width: 100%"></span>
		        </div>
		        <p class="margin-bottom-half">SEO: 100%</p>
	        </div>

        </div>
    </div>

    <div class="row">
        <div class="col span_12">
            <div class="title-medium">
                <h3>Dropcaps</h3>
            </div>
        </div>
    </div>

    <div class="row row-big-col">
        <div class="col span_6">
			<p>
	            <span class="dropcap-gray">A</span> Aliquam erat volutpat. Praesent pellentesque rutrum dolor, in consectetur felis consectetur eget. In lectus metus, hendrerit non fringilla quis, ornare eget urna. Fusce sed vehicula diam. Sed in neque id risus rhoncus viverra a ac nisi. Quisque tempus dictum ante, a suscipit metus tincidunt sed.
	        </p>
        </div>

        <div class="col span_6">
        	<p>
	            <span class="dropcap-green">A</span> Aliquam erat volutpat. Praesent pellentesque rutrum dolor, in consectetur felis consectetur eget. In lectus metus, hendrerit non fringilla quis, ornare eget urna. Fusce sed vehicula diam. Sed in neque id risus rhoncus viverra a ac nisi. Quisque tempus dictum ante, a suscipit metus tincidunt sed.
	        </p>
        </div>
    </div>

    <div class="row">
        <div class="col span_12">
            <div class="title-medium">
                <h3>Blockquote</h3>
            </div>
        </div>
    </div>

    <div class="row row-big-col">
        <div class="col span_12">
			<div class="blockquote-container">
	            <blockquote>
	                <p class="text-italic">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
	            </blockquote>
	        </div>
        </div>
    </div>

    <div class="row">
        <div class="col span_12">
            <div class="title-medium">
                <h3>Highlight & Text forms</h3>
            </div>
        </div>
    </div>

    <div class="row row-big-col">
        <div class="col span_12">
			<span class="green-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span><span>Enim, illo, architecto ipsum voluptate nostrum non quo tempore quisquam.</span><span class="underline">Itaque autem sit perferendis numquam aliquid molestias dicta harum iste aperiam!</span><span>Lorem ipsum dolor sit amet.</span><span class="text-bold">Consequuntur, eaque, consequatur sed.</span><span>At provident saepe ipsum numquam pariatur eum dolorum distinctio deleniti.</span><span class="green">Voluptates iure aperiam sit placeat maxime. Atque!</span>
        </div>
    </div>

    <div class="row">
        <div class="col span_12">
            <div class="title-medium">
                <h3>Table</h3>
            </div>
        </div>
    </div>

    <div class="row row-big-col">
        <div class="col span_12">
            <table class="default-table">
                <thead>
                <tr>
                    <th>Item name</th>
                    <th>Item description</th>
                    <th>Price 1</th>
                    <th>Price 2</th>
                    <th>Price 3</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Item 1</td>
                    <td>Description of item 1</td>
                    <td>$150</td>
                    <td>$300</td>
                    <td>$500</td>
                </tr>
                <tr>
                    <td>Item 2</td>
                    <td>Description of item 2</td>
                    <td>$150</td>
                    <td>$300</td>
                    <td>$500</td>
                </tr>
                <tr>
                    <td>Item 3</td>
                    <td>Description of item 3</td>
                    <td>$150</td>
                    <td>$300</td>
                    <td>$500</td>
                </tr>
                <tr>
                    <td>Item 4</td>
                    <td>Description of item 4</td>
                    <td>$150</td>
                    <td>$300</td>
                    <td>$500</td>
                </tr>
                <tr>
                    <td>Item 5</td>
                    <td>Description of item 5</td>
                    <td>$150</td>
                    <td>$300</td>
                    <td>$500</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col span_12">
            <div class="title-medium">
                <h3>Pricing table</h3>
            </div>
        </div>
    </div>

    <div class="row row-big-col">
        <div class="col span_12">
			<div class="gray-pricing-table pt-four clr">

                <div class="plan">
                    <h3 class="text-bold">Standard</h3>
                    <h4 class="text-bold">$19.99</h4>
                    <ul>
                        <li>5 projects</li>
                        <li>5 GB Storage</li>
                        <li>Ultimated user</li>
                        <li>10 GB Bandwith</li>
                        <li>Enhanced security</li>
                        <li class="pt-button-normal"><a class="text-bold pricing_table_signup" href="#">SIGN UP</a>
                        </li>
                    </ul>
                    <div class="pricing-table-shadow"></div>
                </div>

                <div class="plan">
                    <h3 class="text-bold">Standard</h3>
                    <h4 class="text-bold">$19.99</h4>
                    <ul>
                        <li>5 projects</li>
                        <li>5 GB Storage</li>
                        <li>Ultimated user</li>
                        <li>10 GB Bandwith</li>
                        <li>Enhanced security</li>
                        <li class="pt-button-normal"><a class="text-bold pricing_table_signup" href="#">SIGN UP</a>
                        </li>
                    </ul>
                    <div class="pricing-table-shadow"></div>
                </div>

                <div class="plan">
                    <h3 class="text-bold">Standard</h3>
                    <h4 class="text-bold">$19.99</h4>
                    <ul>
                        <li>5 projects</li>
                        <li>5 GB Storage</li>
                        <li>Ultimated user</li>
                        <li>10 GB Bandwith</li>
                        <li>Enhanced security</li>
                        <li class="pt-button-large"><a class="text-bold pricing_table_signup" href="#">SIGN UP</a></li>
                    </ul>
                    <div class="pricing-table-shadow"></div>
                </div>

                <div class="plan">
                    <h3 class="text-bold">Standard</h3>
                    <h4 class="text-bold">$19.99</h4>
                    <ul>
                        <li>5 projects</li>
                        <li>5 GB Storage</li>
                        <li>Ultimated user</li>
                        <li>10 GB Bandwith</li>
                        <li>Enhanced security</li>
                        <li class="pt-button-normal"><a class="text-bold pricing_table_signup" href="#">SIGN UP</a>
                        </li>
                    </ul>
                    <div class="pricing-table-shadow"></div>
                </div>

            </div>

        </div>
    </div>    

</section>

<?php include 'footer.php'; ?>
