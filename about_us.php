<?php include 'header.php'; ?>

<section class="title-large">
    <div class="title-large-inner">
        <div class="container clr">
            <div class="row span_12 no-margin-col">

                <div class="col span_6">
                    <h2>About<span class="text-bold"> Us</span></h2>                    
                </div>

                <div class="col span_6">
                    <ul class="inline-ul breadcrumb">
                        <li><a href="#">Pages</a></li>
                        <li><a href="about_us.php">About us</a></li>                        
                    </ul>
                </div>

            </div>
        </div>      
    </div>            
</section>

<section class="container clr">

    <div class="row row-big-col">
        <div class="col span_12">
            <h3 class="text-center no-margin tif-text">We are a team of <span class="contact-green">Rubidium Style</span> , we love the minimalist and clean responsive WordPress and HTML themes.</h3>
            <h3 class="text-center tif-text">We set new standards in user experience & make future happen.</h3>
        </div>
    </div>
    
    <div class="row row-big-col row-big-col row-big-col">
        <div class="col span_5">
            <section class="slider">
                <div class="flexslider about-us-slider">
                  <ul class="slides">
                    <li>
                        <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/about-us/about_us_slider_1.jpg" alt="about_us_slider_1">
                    </li>
                    <li>
                        <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/about-us/about_us_slider_2.jpg" alt="about_us_slider_2">
                    </li>
                  </ul>
                </div>
            </section>
        </div>
        <div class="col span_7">
			<div class="title-medium">
                <h3>Welcome to Absolution!</h3>                
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod officiis fugiat maiores repudiandae id expedita corrupti minus perferendis numquam amet sunt explicabo quas nisi ipsam veritatis laudantium perspiciatis sequi cum voluptas aspernatur sint earum nostrum ipsa sapiente quos nulla quis incidunt neque eligendi minima hic similique dolorum quo aliquam inventore.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi aliquid dolorum ad consequatur libero quisquam odio molestiae sapiente deserunt aliquam!</p>
        </div>
    </div>

    <div class="row">
        <div class="col span_12">
            <div class="title-medium">
                <h3>Absolution mission & Our Skill</h3>                
            </div>
        </div>
    </div>

    <div class="row row-big-col row-big-col">
        <div class="col span_6">

        	<div class="accordion clr">
                <div class="title">More Questions?</div>
                <div class="inner">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dictum viverra enim vitae malesuada. Cras nec odio urna, sed posuere neque. Donec scelerisque, diam non placerat tempus, ligula sem vehicula.</p>
                </div>
            </div>

            <div class="accordion clr">
                <div class="title">More Questions?</div>
                <div class="inner">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dictum viverra enim vitae malesuada. Cras nec odio urna, sed posuere neque. Donec scelerisque, diam non placerat tempus, ligula sem vehicula.</p>
                </div>
            </div>

            <div class="accordion clr">
                <div class="title">More Questions?</div>
                <div class="inner">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dictum viverra enim vitae malesuada. Cras nec odio urna, sed posuere neque. Donec scelerisque, diam non placerat tempus, ligula sem vehicula.</p>
                </div>
            </div>

            <div class="accordion clr">
                <div class="title">More Questions?</div>
                <div class="inner">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dictum viverra enim vitae malesuada. Cras nec odio urna, sed posuere neque. Donec scelerisque, diam non placerat tempus, ligula sem vehicula.</p>
                </div>
            </div>

        </div>
        <div class="col span_6">
			<div class="progress-bar margin-bottom">				
		        <div class="meter">
		            <span style="width: 60%"></span>
		        </div>
		        <p class="margin-bottom-half">Web design: 60%</p>
			</div>

			<div class="progress-bar margin-bottom">		        
		        <div class="meter">
		            <span style="width: 80%"></span>
		        </div>
		        <p class="margin-bottom-half">WordPress: 80%</p>
			</div>

			<div class="progress-bar margin-bottom">		        
		        <div class="meter">
		            <span style="width: 40%"></span>
		        </div>
		        <p class="margin-bottom-half">Branding: 40%</p>
	        </div>

			<div class="progress-bar margin-bottom">		        
		        <div class="meter">
		            <span style="width: 90%"></span>
		        </div>
		        <p class="margin-bottom-half">Logo Design: 90%</p>
	        </div>

	        <div class="progress-bar margin-bottom">		        
		        <div class="meter">
		            <span style="width: 100%"></span>
		        </div>
		        <p class="margin-bottom-half">SEO: 100%</p>
	        </div>
        </div>

    </div>

    <div class="row">
        <div class="col span_12">
			<div class="title-medium">
                <h3>Meet the team</h3>                
            </div>
        </div>
    </div>

	<div class="row row-big-col">
		<div class="col span_3">
			<div class="team-member">
				<img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/about-us/tom.jpg" alt="tom">
				<p class="text-bold no-margin">John Doe</p>
				<p class="moon-gray-text">Web Designer and Developer</p>
                <div class="divider-large small-corr"></div>
				<section class="title-large-bottom"></section>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit nobis tempore et maiores asperiores corporis ea.</p>
				<div class="footer-social-icons small clr">
                        <a href="#">
                            <div class="social-icon-dribbble"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-facebook"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-skype"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-twitter"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-vimeo"></div>
                        </a>
                </div>
			</div>
		</div>
		<div class="col span_3">
			<div class="team-member">
                <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/about-us/lisa.jpg" alt="lisa">
                <p class="text-bold no-margin">John Doe</p>
                <p class="moon-gray-text">Web Designer and Developer</p>
                <div class="divider-large small-corr"></div>
                <section class="title-large-bottom"></section>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit nobis tempore et maiores asperiores corporis ea.</p>                <div class="footer-social-icons small clr">
                        <a href="#">
                            <div class="social-icon-dribbble"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-facebook"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-skype"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-twitter"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-vimeo"></div>
                        </a>
                </div>
            </div>
		</div>
		<div class="col span_3">
			<div class="team-member">
                <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/about-us/joe.jpg" alt="joe">
                <p class="text-bold no-margin no-margin">John Doe</p>
                <p class="moon-gray-text">Web Designer and Developer</p>
                <div class="divider-large small-corr"></div>
                <section class="title-large-bottom"></section>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit nobis tempore et maiores asperiores corporis ea.</p>
                <div class="footer-social-icons small clr">
                        <a href="#">
                            <div class="social-icon-dribbble"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-facebook"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-skype"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-twitter"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-vimeo"></div>
                        </a>
                </div>
            </div>
		</div>
		<div class="col span_3">
			<div class="team-member">
                <img src="http://rubidium-style.com/theme-forest-connect/absolution-nbv34m-private/about-us/jasmine.jpg" alt="jasmine">
                <p class="text-bold no-margin">John Doe</p>
                <p class="moon-gray-text">Web Designer and Developer</p>
                <div class="divider-large small-corr"></div>
                <section class="title-large-bottom"></section>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit nobis tempore et maiores asperiores corporis ea.</p>
                <div class="footer-social-icons small clr">
                        <a href="#">
                            <div class="social-icon-dribbble"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-facebook"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-skype"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-twitter"></div>
                        </a>

                        <a href="#">
                            <div class="social-icon-vimeo"></div>
                        </a>
                </div>
            </div>
		</div>
    </div>

	<div class="row">
	    <div class="col span_12">
			<div class="title-medium">
	            <h3>Meet the team</h3>                
	        </div>
	    </div>
	</div>

	<div class="row row-big-col">
	    <div class="col span_12">
			<?php include 'clients.php'; ?>
	    </div>
	</div>

</section>

<?php include 'footer.php'; ?>