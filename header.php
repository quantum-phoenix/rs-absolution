<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
    <meta name="robots" content="all">

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <title>Absolution</title>

    <link rel="stylesheet" type="text/css" href="css/reset.css"/>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800">
	<link rel="stylesheet" type="text/css" href="css/typography.css"/>
	<link rel="stylesheet" type="text/css" href="css/global.css"/>
    <link rel="stylesheet" type="text/css" href="css/elements.css"/>
    <link rel="stylesheet" type="text/css" href="css/shortcodes.css"/>
    <link rel="stylesheet" type="text/css" href="css/widgets.css"/>
    <link rel="stylesheet" type="text/css" href="css/menu.css"/>

    <link rel="stylesheet" type="text/css" href="css/jquery.flexslider.css"/>

    <link rel="stylesheet" type="text/css" href="css/revolution/settings.css"/>
    <link rel="stylesheet" type="text/css" href="css/revolution/fullwidth.css"/>
    <link rel="stylesheet" type="text/css" href="css/revolution/responsive.css"/>
    <link rel="stylesheet" type="text/css" href="css/revolution/absolution_revolution.css"/>
   
    <link rel="stylesheet" type="text/css" href="css/jquery.tweet.css"/>

    <link rel="stylesheet" type="text/css" href="css/jquery.isotope.css"/>
    <link rel="stylesheet" type="text/css" href="css/jquery.jcarousel.css"/>

    <link rel="stylesheet" type="text/css" href="css/main.css"/>
    <link rel="stylesheet" type="text/css" href="css/responsive-gs-12col.css"/>
    <link rel="stylesheet" type="text/css" href="css/mediaqueries.css"/>

    <!-- <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script> -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=AIzaSyBurMIbLiO3XOdziJ1fkD-sP_KXtF_r_uk"></script>
    <script type="text/javascript" src="js/jquery.gmap-1.1.0-min.js"></script>
    <script type="text/javascript" src="js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="js/jquery.easing-1.3.min.js"></script>
    <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>  
    <script type="text/javascript" src="js/jquery.jcarousel.min.js"></script>
    <script type="text/javascript" src="js/modernizr.custom.17475.js"></script>
    <script type="text/javascript" src="js/jquery.mobilemenu.js"></script>
    <script type="text/javascript" src="js/jquery.tweet.js"></script>
    
    <script type="text/javascript" src="js/main.js"></script>    
</head>
<body>

<header class="header-top">
    <div class="container clr">
        <div class="row no-margin-col">
            <div class="col span_5">
                <div class="floatleft">
                    <ul class="inline-ul no-margin">
                        <li><p class="phone-top nova-text">Call us: (20) 3888-234</p></li>
                        <li><p class="mail-top nova-text">sample@sample.com</p></li>
                    </ul>          
                </div>
            </div>

            <div class="col span_7">
                <div class="header-social-icons-parent">
                    <div class="header-social-icons clr">
                        <a href="#">
                            <div class="social-icon-dribbble"></div>
                        </a>
                        <a href="#">
                            <div class="social-icon-facebook"></div>
                        </a>
                        <a href="#">
                            <div class="social-icon-skype"></div>
                        </a>
                        <a href="#">
                            <div class="social-icon-twitter"></div>
                        </a>
                        <a href="#">
                            <div class="social-icon-vimeo"></div>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>  
</header>

<header class="header-container">
    <div class="header-elements container">
        <div class="row span_12 no-margin-col">

            <div class="col span_3">
                <div class="logo">
                    <img src="logo.png" alt="logo">
                </div>
            </div>

            <div class="col span_9">
                <nav class="menu-system">
                    <ul class="absolution">
                        <li>
                            <a href="home.php">
                                <span class="icon"><div class="menu-icon menu-icon-home"></div></span>HOME
                            </a>
                        </li>

                        <li>
                            <a href="index.php">
                                <span class="icon"><div class="menu-icon menu-icon-blog"></div></span>BLOG
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <span class="icon"><div class="menu-icon menu-icon-pages"></div></span>PAGES
                            </a>
                            <ul>
                                <li><a href="about_us.php">About Us</a></li>
                                <li><a href="shortcodes.php">Shortcodes</a></li>
                                <li><a href="our_services.php">Our services</a></li>
                            </ul>
                        </li>
                        
                        <li>
                            <a href="portfolio.php">
                                <span class="icon"><div class="menu-icon menu-icon-portfolio"></div></span>PORTFOLIO
                            </a>
                            <ul>
                                <li><a href="single_portfolio.php">Single Portfolio</a></li>
                            </ul>
                        </li>                        
                        <li>
                            <a href="contact.php">
                                <span class="icon"><div class="menu-icon menu-icon-contact"></div></span>CONTACT
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>

        </div>
    </div>
</header>